# CSI30 - Sistemas Inteligentes - 2019/1

## Sobre
Trabalho feito por Amir Leonardo Kessler Annahas e Mateus Capelesso, para a disciplina CSI30 Sistemas Inteligentes da UTFPR, do ano de 2019/1.

## Como instalar

Baixe ou clone este repositorio em seu computador. Abra o terminal e navegue até este diretório. Execute o comando `npm install` neste diretório. Isto instalará os componentes necessários para compilação do código.

## Como compilar

Após instalar os componentes necessários, no mesmo diretório execute o comando `npm start`, isto compilará o código. Se compilado com sucesso, o programa estará rodando na porta 4200.

## Como visualizar a aplicação

Abra seu navegador e navegue até `http://localhost:4200/`. Ao abrir a página dos trabalhos relativos a Sistemas Inteligentes, clique em `Busca no Grid`. Na tela seguinte, você poderá compor o grid como desejar. No dropdown pode ser escolhido o tipo de elemento que você que utilizar. Podendo ser:

- Obstáculo - Posições nas quais agente não pode acessar;
- Agente - Origem do movimento;
- Destino - Objetivo do agente;
- Vazio - Elimina o que havia na posição;

Estão implementados o algoritmo de Dijkstra, *(AStar) e algoritmo genético (onde é encontrado até 3 pontos intermediários para o caminho mínimo). Para executá-los, basta povoar o grid com o agente, destino e os obstáculos necessários, e clicar em um dos três botões. No grid acontecerá a animação referente ao algoritmo selecionado. Para limpar todo o grid, basta alterar seu tamanho.