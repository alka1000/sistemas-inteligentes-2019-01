import { Estado } from './models/estado';
import { Conteudo } from './enum/conteudo';
import { Grid } from './models/grid';
import { Component, OnInit, NgZone } from '@angular/core';
import { Face } from './enum/face';
import 'setimmediate';

@Component({
  selector: 'app-busca-no-grid',
  templateUrl: './busca-no-grid.component.html',
  styleUrls: ['./busca-no-grid.component.css']
})
export class BuscaNoGridComponent implements OnInit {
  protected grid: Grid;
  protected imgClass: string;
  protected agente: Estado;
  protected objetivo: Estado;
  protected insert: Conteudo = Conteudo.OBSTACULO;
  protected conteudos = [
    Conteudo.VAZIO,
    Conteudo.AGENTE,
    Conteudo.OBJETIVO,
    Conteudo.OBSTACULO
  ];
  protected xLength = 10;
  protected yLength = 10;

  constructor(
    private ngZone: NgZone
  ) { }

  ngOnInit() {
    this.updateGrid();
  }

  protected genetico() {

    for (let i = 0; i < this.grid.xLength; i++) {
      for (let j = 0; j < this.grid.yLength; j++) {
        if (this.grid.grid[j][i] === Conteudo.VISITADO || this.grid.grid[j][i] === Conteudo.CAMINHO_MINIMO) {
          this.grid.grid[j][i] = Conteudo.VAZIO;
        }
      }
    }

    const Genetical = require('genetical');

    const options = {
      populationSize: 100,
      populationFactory: this.populationFactory,
      terminationCondition: this.terminationCondition,
      fitnessEvaluator: this.fitnessEvaluator,
      natural: false, // Menor nota, melhor o candidato.
      selectionStrategy: Genetical.ROULETTEWHEELSELECTION,
      evolutionOptions: {
          crossover: this.crossover,
          crossoverPoints: 1,
          mutate: this.mutate,
          mutationProbability : 0.02
      }
    };

    process.hrtime = require('browser-process-hrtime');

    const ga = new Genetical(options);
    ga.on('initial population created', (initialPopulation) => {
      initialPopulation.forEach(individuo => {
        individuo.contexto = this;
      });
    });
    ga.solve((bestCandidate, generation) => {
      this.ngZone.run(() => {
        if (bestCandidate.a[0] !== this.xLength && bestCandidate.a[1] !== this.yLength) {
          this.atualizaQuadradinhos(bestCandidate.a[0], bestCandidate.a[1]);
        }
        if (bestCandidate.b[0] !== this.xLength && bestCandidate.b[1] !== this.yLength) {
          this.atualizaQuadradinhos(bestCandidate.b[0], bestCandidate.b[1]);
        }
        if (bestCandidate.c[0] !== this.xLength && bestCandidate.c[1] !== this.yLength) {
          this.atualizaQuadradinhos(bestCandidate.c[0], bestCandidate.c[1]);
        }
      });
      console.log('Best Candidate', bestCandidate, 'Generation', generation);
    });
  }

  protected atualizaQuadradinhos(x, y) {
    this.grid.grid[y][x] = Conteudo.CAMINHO_MINIMO;
  }

  protected terminationCondition(stats) {
    return stats.generation === 50;
  }

  protected mutate(candidate, mutationProbability, generator, callback) {
    while (
        (candidate.a[0] === candidate.contexto.xLength
        || candidate.a[1] === candidate.contexto.yLength)
        && (candidate.b[0] === candidate.contexto.xLength
        || candidate.b[1] === candidate.contexto.yLength)
        && (candidate.c[0] === candidate.contexto.xLength
        || candidate.c[1] === candidate.contexto.yLength)
      ) {
      candidate = {
        a: [ this.getRandomInt(0, candidate.contexto.xLength, generator), this.getRandomInt(0, candidate.contexto.yLength, generator) ],
        b: [ this.getRandomInt(0, candidate.contexto.xLength, generator), this.getRandomInt(0, candidate.contexto.yLength, generator) ],
        c: [ this.getRandomInt(0, candidate.contexto.xLength, generator), this.getRandomInt(0, candidate.contexto.yLength, generator) ]
      };
    }

    callback(candidate);
  }

  protected crossover(parent1, parent2, points, generator, callback) {
    let child = {
      a: [ parent1.a[0], parent2.a[1] ],
      b: [ parent1.b[0], parent2.b[1] ],
      c: [ parent1.c[0], parent2.c[1] ],
      contexto: parent1.contexto
    };
    while (
      (child.a[0] === parent1.contexto.xLength
      || child.a[1] === parent1.contexto.yLength)
      && (child.b[0] === parent1.contexto.xLength
      || child.b[1] === parent1.contexto.yLength)
      && (child.c[0] === parent1.contexto.xLength
      || child.c[1] === parent1.contexto.yLength)
    ) {
      // caso seja um gene inválido, aleatoriza ele
      child = {
        a: [
          parent1.contexto.getRandomInt(0, parent1.contexto.xLength, generator),
          parent1.contexto.getRandomInt(0, parent1.contexto.yLength, generator)
        ],
        b: [
          parent1.contexto.getRandomInt(0, parent1.contexto.xLength, generator),
          parent1.contexto.getRandomInt(0, parent1.contexto.yLength, generator)
        ],
        c: [
          parent1.contexto.getRandomInt(0, parent1.contexto.xLength, generator),
          parent1.contexto.getRandomInt(0, parent1.contexto.yLength, generator)
        ],
        contexto: parent1.contexto
      };
    }
    return callback([child]);
  }

  protected populationFactory(population, populationSize, generator, callback) {
    let candidate = {
      a: [ 10, 10 ],
      b: [ 10, 10 ],
      c: [ 10, 10 ]
    };
    while (
        (candidate.a[0] === 10
        || candidate.a[1] === 10)
        && (candidate.b[0] === 10
        || candidate.b[1] === 10)
        && (candidate.c[0] === 10
        || candidate.c[1] === 10)
      ) {
      candidate = {
        a: [ this.getRandomInt(0, 10, generator), this.getRandomInt(0, 10, generator) ],
        b: [ this.getRandomInt(0, 10, generator), this.getRandomInt(0, 10, generator) ],
        c: [ this.getRandomInt(0, 10, generator), this.getRandomInt(0, 10, generator) ]
      };
    }

    return callback(null, candidate);
  }

  protected fitnessEvaluator(candidate, callback) {
    let fit = 0;
    const grid = new Grid (candidate.contexto.xLength, candidate.contexto.yLength);

    const astarFitness = (xLength, yLength, start, end, gridReal) => {

      const simulaAndarFitness = (obj: Estado) => {
        let targety = obj.y;
        let targetx = obj.x;
        switch (obj.face) {
          case Face.NORTE:
            targety = obj.y - 1;
            break;
          case Face.NORDESTE:
            targety = obj.y - 1;
            targetx = obj.x + 1;
            break;
          case Face.LESTE:
            targetx = obj.x + 1;
            break;
          case Face.SUDESTE:
            targetx = obj.x + 1;
            targety = obj.y + 1;
            break;
          case Face.SUL:
            targety = obj.y + 1;
            break;
          case Face.SUDOESTE:
            targety = obj.y + 1;
            targetx = obj.x - 1;
            break;
          case Face.OESTE:
            targetx = obj.x - 1;
            break;
          case Face.NOROESTE:
            targetx = obj.x - 1;
            targety = obj.y - 1;
            break;
        }
        if (targety < 0
          || targety >= yLength
          || targetx < 0
          || targetx >= xLength
        ) {
          targety = obj.y;
          targetx = obj.x;
        }
        return new Estado(obj.face, targetx, targety, Number.MAX_VALUE, Number.MAX_VALUE, obj);
      };

      const visitados: Estado[] = [];
      const naoVisitados: Estado[] = [];
      let atual = start;
      atual.custo = 0;
      atual.passosDados = 0;
      atual.estadoAnterior = null;
      while (true) {
        visitados.push(atual);

        if (atual.x === end.x && atual.y === end.y) {
          let fitness = atual.custo;
          while (atual.estadoAnterior != null) {
            if (gridReal.grid[atual.y][atual.x] === Conteudo.OBSTACULO) {
              fitness += 50;
            }
            atual = atual.estadoAnterior;
          }

          return fitness;
        }

        // retira estado atual dos gerados
        for (let i = 0; i < naoVisitados.length; i++) {
          if (naoVisitados[i].equals(atual)) {
            naoVisitados.splice(i, 1);
          }
        }

        // Calcula distancia para adiciona-la ao custo do nodo, para andar para os com menos custo
        // h(n) = sqrt((x1 - x2)2 + (y1 - y2)2)
        let distancia = Math.sqrt(Math.pow(atual.x - end.x, 2) + Math.pow(atual.y - end.y, 2));

        let estadoPossivel = new Estado(
          atual.simulaGirarHorario(),
          atual.x,
          atual.y,
          atual.passosDados + distancia + 1,
          atual.passosDados + 1,
          atual
        );
        // se estado nao foi visitado
        if (!visitados.find(estAux => estAux.equals(estadoPossivel))) {
          // verifica se o estado ja foi gerado, se foi atualiza o custo, senao gera ele
          if (!naoVisitados.find(estAux => {
            if (estAux.equals(estadoPossivel)) {
              if (estadoPossivel.custo < estAux.custo) {
                estAux = estadoPossivel;
              }
              return true;
            }
          })) {
            naoVisitados.push(estadoPossivel);
          }
        }
        estadoPossivel = new Estado(
          atual.simulaGirarAntiHorario(),
          atual.x,
          atual.y,
          atual.passosDados + distancia + 1,
          atual.passosDados + 1,
          atual
        );
        // se estado nao foi visitado
        if (!visitados.find(estAux => estAux.equals(estadoPossivel))) {
          // verifica se o estado ja foi gerado, se foi atualiza o custo, senao gera ele
          if (!naoVisitados.find(estAux => {
            if (estAux.equals(estadoPossivel)) {
              if (estadoPossivel.custo < estAux.custo) {
                estAux = estadoPossivel;
              }
              return true;
            }
          })) {
            naoVisitados.push(estadoPossivel);
          }
        }

        estadoPossivel = simulaAndarFitness(atual);
        distancia = Math.sqrt(Math.pow(estadoPossivel.x - end.x, 2) + Math.pow(estadoPossivel.y - end.y, 2));
        estadoPossivel.custo = atual.passosDados + distancia + 1;
        estadoPossivel.passosDados = atual.passosDados + 1;
        // se estado nao foi visitado
        if (!visitados.find(estAux => estAux.equals(estadoPossivel))) {
          // verifica se o estado ja foi gerado, se foi atualiza o custo, senao gera ele
          if (!naoVisitados.find(estAux => {
            if (estAux.equals(estadoPossivel)) {
              if (estadoPossivel.custo < estAux.custo) {
                estAux = estadoPossivel;
              }
              return true;
            }
          })) {
            naoVisitados.push(estadoPossivel);
          }
        }

        naoVisitados.sort((e1, e2) => {
          if (e1.custo > e2.custo) {
            return 1;
          }
          if (e1.custo < e2.custo) {
            return -1;
          }
          return 0;
        });

        atual = naoVisitados[0];

      }
    };
    const agente = new Estado(
      Face.LESTE,
      candidate.contexto.agente.x,
      candidate.contexto.agente.y,
      0,
      0,
      null
    );

    if (candidate.a[0] === candidate.contexto.xLength || candidate.a[1] === candidate.contexto.yLength) {
      if (candidate.b[0] === candidate.contexto.xLength || candidate.b[1] === candidate.contexto.yLength) {
        // Não existe o ponto A e B
        const pontoC = new Estado(
          Face.LESTE,
          candidate.c[0],
          candidate.c[1],
          0,
          0,
          null
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          agente,
          pontoC,
          candidate.contexto.grid
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          pontoC,
          candidate.contexto.objetivo,
          candidate.contexto.grid
        );
      } else if (candidate.c[0] === candidate.contexto.xLength || candidate.c[1] === candidate.contexto.yLength) {
        // Não existe o ponto A e C
        const pontoB = new Estado(
          Face.LESTE,
          candidate.b[0],
          candidate.b[1],
          0,
          0,
          null
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          agente,
          pontoB,
          candidate.contexto.grid
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          pontoB,
          candidate.contexto.objetivo,
          candidate.contexto.grid
        );
      } else {
        // Não existe o ponto A
        const pontoB = new Estado(
          Face.LESTE,
          candidate.b[0],
          candidate.b[1],
          0,
          0,
          null
        );
        const pontoC = new Estado(
          Face.LESTE,
          candidate.c[0],
          candidate.c[1],
          0,
          0,
          null
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          agente,
          pontoB,
          candidate.contexto.grid
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          pontoB,
          pontoC,
          candidate.contexto.grid
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          pontoC,
          candidate.contexto.objetivo,
          candidate.contexto.grid
        );
      }
    } else if (candidate.b[0] === candidate.contexto.xLength || candidate.b[1] === candidate.contexto.yLength) {
      if (candidate.c[0] === candidate.contexto.xLength || candidate.c[1] === candidate.contexto.yLength) {
        // Não existe o ponto B e C
        const pontoA = new Estado(
          Face.LESTE,
          candidate.a[0],
          candidate.a[1],
          0,
          0,
          null
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          agente,
          pontoA,
          candidate.contexto.grid
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          pontoA,
          candidate.contexto.objetivo,
          candidate.contexto.grid
        );
      } else {
        // Não existe o ponto B
        const pontoA = new Estado(
          Face.LESTE,
          candidate.a[0],
          candidate.a[1],
          0,
          0,
          null
        );
        const pontoC = new Estado(
          Face.LESTE,
          candidate.c[0],
          candidate.c[1],
          0,
          0,
          null
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          agente,
          pontoA,
          candidate.contexto.grid
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          pontoA,
          pontoC,
          candidate.contexto.grid
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          pontoC,
          candidate.contexto.objetivo,
          candidate.contexto.grid
        );
      }
    } else if (candidate.c[0] === candidate.contexto.xLength || candidate.c[1] === candidate.contexto.yLength) {
      // Não existe o ponto C
        const pontoB = new Estado(
          Face.LESTE,
          candidate.b[0],
          candidate.b[1],
          0,
          0,
          null
        );
        const pontoA = new Estado(
          Face.LESTE,
          candidate.a[0],
          candidate.a[1],
          0,
          0,
          null
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          agente,
          pontoA,
          candidate.contexto.grid
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          pontoA,
          pontoB,
          candidate.contexto.grid
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          pontoB,
          candidate.contexto.objetivo,
          candidate.contexto.grid
        );
    } else {
      // Existem todos os pontos
        const pontoA = new Estado(
          Face.LESTE,
          candidate.a[0],
          candidate.a[1],
          0,
          0,
          null
        );
        const pontoB = new Estado(
          Face.LESTE,
          candidate.b[0],
          candidate.b[1],
          0,
          0,
          null
        );
        const pontoC = new Estado(
          Face.LESTE,
          candidate.c[0],
          candidate.c[1],
          0,
          0,
          null
        );

        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          agente,
          pontoA,
          candidate.contexto.grid
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          pontoA,
          pontoB,
          candidate.contexto.grid
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          pontoB,
          pontoC,
          candidate.contexto.grid
        );
        fit += astarFitness(
          candidate.contexto.xLength,
          candidate.contexto.yLength,
          pontoC,
          candidate.contexto.objetivo,
          candidate.contexto.grid
        );
    }
    return callback(null, fit);
  }

  protected getImg(conteudo: Conteudo, x: number, y: number): string {
    if (this.agente.x === x && this.agente.y === y) {
      return 'assets/img/busca-no-grid/arrow.png';
    }
    if (conteudo === Conteudo.OBSTACULO) {
      return 'assets/img/busca-no-grid/blocked.png';
    }
    if (this.objetivo.x === x && this.objetivo.y === y) {
      return 'assets/img/busca-no-grid/goal.png';
    }

    return 'assets/img/busca-no-grid/blocked.png';

  }


  protected getClassImg(conteudo: Conteudo, x: number, y: number): string {
    if (conteudo === Conteudo.OBSTACULO) {
      return 'blocked';
    }
    if (conteudo === Conteudo.VISITADO) {
      if (this.agente.x === x && this.agente.y === y) {
        switch (this.agente.face) {
          case Face.NORTE:
            return 'visited agente-norte';
          case Face.NORDESTE:
            return 'visited agente-nordeste';
          case Face.LESTE:
            return 'visited agente-leste';
          case Face.SUDESTE:
            return 'visited agente-sudeste';
          case Face.SUL:
            return 'visited agente-sul';
          case Face.SUDOESTE:
            return 'visited agente-sudoeste';
          case Face.OESTE:
            return 'visited agente-oeste';
          case Face.NOROESTE:
            return 'visited agente-noroeste';
          default:
            return 'visited';
        }
      } else {
        return 'visited';
      }
    }
    if (conteudo === Conteudo.CAMINHO_MINIMO) {
      if (this.agente.x === x && this.agente.y === y) {
        switch (this.agente.face) {
          case Face.NORTE:
            return 'caminho-minimo agente-norte';
          case Face.NORDESTE:
            return 'caminho-minimo agente-nordeste';
          case Face.LESTE:
            return 'caminho-minimo agente-leste';
          case Face.SUDESTE:
            return 'caminho-minimo agente-sudeste';
          case Face.SUL:
            return 'caminho-minimo agente-sul';
          case Face.SUDOESTE:
            return 'caminho-minimo agente-sudoeste';
          case Face.OESTE:
            return 'caminho-minimo agente-oeste';
          case Face.NOROESTE:
            return 'caminho-minimo agente-noroeste';
          default:
            return 'caminho-minimo';
        }
      } else {
        return 'caminho-minimo';
      }
    }
    if (this.objetivo.x === x && this.objetivo.y === y) {
      return 'objetivo';
    }
    if (this.agente.x === x && this.agente.y === y) {
      switch (this.agente.face) {
        case Face.NORTE:
          return 'agente-norte';
        case Face.NORDESTE:
          return 'agente-nordeste';
        case Face.LESTE:
          return 'agente-leste';
        case Face.SUDESTE:
          return 'agente-sudeste';
        case Face.SUL:
          return 'agente-sul';
        case Face.SUDOESTE:
          return 'agente-sudoeste';
        case Face.OESTE:
          return 'agente-oeste';
        case Face.NOROESTE:
          return 'agente-noroeste';
      }
    }
    return 'd-none';
  }


  protected toggle(x, y) {
    if (this.insert === Conteudo.AGENTE) {
      this.agente.x = x;
      this.agente.y = y;
    } else if (this.agente.x === x && this.agente.y === y) {
      this.agente.x = -1;
      this.agente.y = -1;
    }
    if (this.insert === Conteudo.OBJETIVO) {
      this.objetivo.x = x;
      this.objetivo.y = y;
    } else if (this.objetivo.x === x && this.objetivo.y === y) {
      this.objetivo.x = -1;
      this.objetivo.y = -1;
    }
    this.grid.grid[y][x] = this.insert;
  }

  protected andar() {
    let targety = this.agente.y;
    let targetx = this.agente.x;
    switch (this.agente.face) {
      case Face.NORTE:
        targety = this.agente.y - 1;
        break;
      case Face.NORDESTE:
        targety = this.agente.y - 1;
        targetx = this.agente.x + 1;
        break;
      case Face.LESTE:
        targetx = this.agente.x + 1;
        break;
      case Face.SUDESTE:
        targetx = this.agente.x + 1;
        targety = this.agente.y + 1;
        break;
      case Face.SUL:
        targety = this.agente.y + 1;
        break;
      case Face.SUDOESTE:
        targety = this.agente.y + 1;
        targetx = this.agente.x - 1;
        break;
      case Face.OESTE:
        targetx = this.agente.x - 1;
        break;
      case Face.NOROESTE:
        targetx = this.agente.x - 1;
        targety = this.agente.y - 1;
        break;
    }

    if (targety >= 0
        && targety < this.grid.yLength
        && targetx >= 0
        && targetx < this.grid.xLength
        && this.grid.grid[targety][targetx] !== Conteudo.OBSTACULO
      ) {
        this.agente.y = targety;
        this.agente.x = targetx;
      }
  }



  protected simulaAndar(atual: Estado): Estado {
    let targety = atual.y;
    let targetx = atual.x;
    switch (atual.face) {
      case Face.NORTE:
        targety = atual.y - 1;
        break;
      case Face.NORDESTE:
        targety = atual.y - 1;
        targetx = atual.x + 1;
        break;
      case Face.LESTE:
        targetx = atual.x + 1;
        break;
      case Face.SUDESTE:
        targetx = atual.x + 1;
        targety = atual.y + 1;
        break;
      case Face.SUL:
        targety = atual.y + 1;
        break;
      case Face.SUDOESTE:
        targety = atual.y + 1;
        targetx = atual.x - 1;
        break;
      case Face.OESTE:
        targetx = atual.x - 1;
        break;
      case Face.NOROESTE:
        targetx = atual.x - 1;
        targety = atual.y - 1;
        break;
    }
    if (targety < 0
      || targety >= this.grid.yLength
      || targetx < 0
      || targetx >= this.grid.xLength
      || this.grid.grid[targety][targetx] === Conteudo.OBSTACULO
    ) {
      targety = atual.y;
      targetx = atual.x;
    }
    return new Estado(atual.face, targetx, targety, Number.MAX_VALUE, Number.MAX_VALUE, atual);
  }

  protected updateGrid() {
    this.grid = new Grid(
      this.yLength,
      this.xLength
    );
    this.agente = new Estado(
      Face.LESTE,
      -1,
      -1,
      0,
      0,
      null
    );
    this.objetivo = new Estado(
      Face.LESTE,
      -1,
      -1,
      Number.MAX_VALUE,
      Number.MAX_VALUE,
      null
    );
  }

  protected dijkstra(): boolean {
    const visitados: Estado[] = [];
    const naoVisitados: Estado[] = [];
    for (let i = 0; i < this.grid.xLength; i++) {
      for (let j = 0; j < this.grid.yLength; j++) {
        if (this.grid.grid[j][i] === Conteudo.VISITADO || this.grid.grid[j][i] === Conteudo.CAMINHO_MINIMO) {
          this.grid.grid[j][i] = Conteudo.VAZIO;
        }
      }
    }
    let atual = this.agente;
    atual.custo = 0;
    atual.estadoAnterior = null;
    while (true) {
      visitados.push(atual);
      // pinta o quadrado visitado
      this.grid.grid[atual.y][atual.x] = Conteudo.VISITADO;

      if (atual.x === this.objetivo.x && atual.y === this.objetivo.y) {
        const caminho: Estado[] = [];
        while (atual.estadoAnterior != null) {
          caminho.push(atual);
          atual = atual.estadoAnterior;
        }
        this.caminhoLoop(caminho);
        return true;
      }

      // retira estado atual dos gerados
      for (let i = 0; i < naoVisitados.length; i++) {
        if (naoVisitados[i].equals(atual)) {
          naoVisitados.splice(i, 1);
        }
      }

      let estadoPossivel = new Estado(atual.simulaGirarHorario(), atual.x, atual.y, atual.custo + 1, 0, atual);
      // se estado nao foi visitado
      if (!visitados.find(estAux => estAux.equals(estadoPossivel))) {
        // verifica se o estado ja foi gerado, se foi atualiza o custo, senao gera ele
        if (!naoVisitados.find(estAux => {
          if (estAux.equals(estadoPossivel)) {
            if (estadoPossivel.custo < estAux.custo) {
              estAux = estadoPossivel;
            }
            return true;
          }
        })) {
          naoVisitados.push(estadoPossivel);
        }
      }
      estadoPossivel = new Estado(atual.simulaGirarAntiHorario(), atual.x, atual.y, atual.custo + 1, 0, atual);
      // se estado nao foi visitado
      if (!visitados.find(estAux => estAux.equals(estadoPossivel))) {
        // verifica se o estado ja foi gerado, se foi atualiza o custo, senao gera ele
        if (!naoVisitados.find(estAux => {
          if (estAux.equals(estadoPossivel)) {
            if (estadoPossivel.custo < estAux.custo) {
              estAux = estadoPossivel;
            }
            return true;
          }
        })) {
          naoVisitados.push(estadoPossivel);
        }
      }

      estadoPossivel = this.simulaAndar(atual);
      estadoPossivel.custo = atual.custo + 1;
      // se estado nao foi visitado
      if (!visitados.find(estAux => estAux.equals(estadoPossivel))) {
        // verifica se o estado ja foi gerado, se foi atualiza o custo, senao gera ele
        if (!naoVisitados.find(estAux => {
          if (estAux.equals(estadoPossivel)) {
            if (estadoPossivel.custo < estAux.custo) {
              estAux = estadoPossivel;
            }
            return true;
          }
        })) {
          naoVisitados.push(estadoPossivel);
        }
      }

      naoVisitados.sort((e1, e2) => {
        if (e1.custo > e2.custo) {
          return 1;
        }
        if (e1.custo < e2.custo) {
          return -1;
        }
        return 0;
      });

      atual = naoVisitados[0];

    }
  }

  protected astar(): boolean {
    const visitados: Estado[] = [];
    const naoVisitados: Estado[] = [];
    for (let i = 0; i < this.grid.xLength; i++) {
      for (let j = 0; j < this.grid.yLength; j++) {
        if (this.grid.grid[j][i] === Conteudo.VISITADO || this.grid.grid[j][i] === Conteudo.CAMINHO_MINIMO) {
          this.grid.grid[j][i] = Conteudo.VAZIO;
        }
      }
    }
    let atual = this.agente;
    atual.custo = 0;
    atual.passosDados = 0;
    atual.estadoAnterior = null;
    while (true) {
      visitados.push(atual);
      // pinta o quadrado visitado
      this.grid.grid[atual.y][atual.x] = Conteudo.VISITADO;

      if (atual.x === this.objetivo.x && atual.y === this.objetivo.y) {
        const caminho: Estado[] = [];
        while (atual.estadoAnterior != null) {
          caminho.push(atual);
          atual = atual.estadoAnterior;
        }
        this.caminhoLoop(caminho);
        return true;
      }

      // retira estado atual dos gerados
      for (let i = 0; i < naoVisitados.length; i++) {
        if (naoVisitados[i].equals(atual)) {
          naoVisitados.splice(i, 1);
        }
      }

      // Calcula distancia para adiciona-la ao custo do nodo, para andar para os com menos custo
      // h(n) = sqrt((x1 - x2)2 + (y1 - y2)2)
      let distancia = Math.sqrt(Math.pow(atual.x - this.objetivo.x, 2) + Math.pow(atual.y - this.objetivo.y, 2));

      let estadoPossivel = new Estado(
        atual.simulaGirarHorario(),
        atual.x,
        atual.y,
        atual.passosDados + distancia + 1,
        atual.passosDados + 1,
        atual
      );
      // se estado nao foi visitado
      if (!visitados.find(estAux => estAux.equals(estadoPossivel))) {
        // verifica se o estado ja foi gerado, se foi atualiza o custo, senao gera ele
        if (!naoVisitados.find(estAux => {
          if (estAux.equals(estadoPossivel)) {
            if (estadoPossivel.custo < estAux.custo) {
              estAux = estadoPossivel;
            }
            return true;
          }
        })) {
          naoVisitados.push(estadoPossivel);
        }
      }
      estadoPossivel = new Estado(
        atual.simulaGirarAntiHorario(),
        atual.x,
        atual.y,
        atual.passosDados + distancia + 1,
        atual.passosDados + 1,
        atual
      );
      // se estado nao foi visitado
      if (!visitados.find(estAux => estAux.equals(estadoPossivel))) {
        // verifica se o estado ja foi gerado, se foi atualiza o custo, senao gera ele
        if (!naoVisitados.find(estAux => {
          if (estAux.equals(estadoPossivel)) {
            if (estadoPossivel.custo < estAux.custo) {
              estAux = estadoPossivel;
            }
            return true;
          }
        })) {
          naoVisitados.push(estadoPossivel);
        }
      }

      estadoPossivel = this.simulaAndar(atual);
      distancia = Math.sqrt(Math.pow(estadoPossivel.x - this.objetivo.x, 2) + Math.pow(estadoPossivel.y - this.objetivo.y, 2));
      estadoPossivel.custo = atual.passosDados + distancia + 1;
      estadoPossivel.passosDados = atual.passosDados + 1;
      // se estado nao foi visitado
      if (!visitados.find(estAux => estAux.equals(estadoPossivel))) {
        // verifica se o estado ja foi gerado, se foi atualiza o custo, senao gera ele
        if (!naoVisitados.find(estAux => {
          if (estAux.equals(estadoPossivel)) {
            if (estadoPossivel.custo < estAux.custo) {
              estAux = estadoPossivel;
            }
            return true;
          }
        })) {
          naoVisitados.push(estadoPossivel);
        }
      }

      naoVisitados.sort((e1, e2) => {
        if (e1.custo > e2.custo) {
          return 1;
        }
        if (e1.custo < e2.custo) {
          return -1;
        }
        return 0;
      });

      atual = naoVisitados[0];

    }
  }


  protected caminhoLoop(caminho: Estado[]): any {
    if (caminho.length > 0) {
      this.agente = caminho.pop();
      this.grid.grid[this.agente.y][this.agente.x] = Conteudo.CAMINHO_MINIMO;
      setTimeout(() => {
        this.caminhoLoop(caminho);
      }, 500);
    }
  }

  protected getRandomInt(min, max, generator) {
    return Math.floor(generator.random() * (max - min + 1)) + min;
  }
}
