import { Conteudo } from './../enum/conteudo';
export class Grid {
  private _xLength: number;
  private _yLength: number;
  private _grid: Conteudo[][] = [];


  constructor(
    xLength: number,
    yLength: number
  ) {
    this.xLength = xLength;
    this.yLength = yLength;

    for (let i = 0; i < yLength; i++) {
      const iList: Conteudo[] = [];
      for (let j = 0; j < xLength; j++) {
        iList.push(Conteudo.VAZIO);
      }
      this.grid.push(iList);
    }
  }


    /**
     * Getter xLength
     */
  public get xLength(): number {
    return this._xLength;
  }

  /**
   * Setter xLength
   */
  public set xLength(value: number) {
    this._xLength = value;
  }


  /**
   * Getter yLength
   */
  public get yLength(): number {
    return this._yLength;
  }

  /**
   * Setter yLength
   */
  public set yLength(value: number) {
    this._yLength = value;
  }


  /**
   * Getter grid
   */
  public get grid(): Conteudo[][] {
    return this._grid;
  }

  /**
   * Setter grid
   */
  public set grid(value: Conteudo[][]) {
    this._grid = value;
  }

}
