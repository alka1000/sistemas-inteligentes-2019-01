import { Face } from './../enum/face';

export class Estado {
  private _face: Face;
  private _x: number;
  private _y: number;
  private _custo: number;
  private _passosDados: number;
  private _estadoAnterior: Estado;


  constructor(
    face: Face,
    x: number,
    y: number,
    custo: number,
    passosDados: number,
    estadoAnterior: Estado
  ) {
    this._face = face;
    this._x = x;
    this._y = y;
    this._custo = custo;
    this._passosDados = passosDados;
    this._estadoAnterior = estadoAnterior;
  }


  /**
   * Getter face
   */
  public get face(): Face {
    return this._face;
  }

  /**
   * Setter face
   */
  public set face(value: Face) {
    this._face = value;
  }

  /**
   * Getter x
   */
  public get x(): number {
    return this._x;
  }

  /**
   * Setter x
   */
  public set x(value: number) {
   this._x = value;
  }

  /**
   * Getter y
   */
  public get y(): number {
    return this._y;
  }

  /**
   * Setter y
   */
  public set y(value: number) {
    this._y = value;
  }

  /**
   * Getter custo
   */
  public get custo(): number {
    return this._custo;
  }

  /**
   * Setter custo
   */
  public set custo(value: number) {
    this._custo = value;
  }


  /**
   * Getter passosDados
   */
  public get passosDados(): number {
    return this._passosDados;
  }

  /**
   * Setter passosDados
   */
  public set passosDados(value: number) {
    this._passosDados = value;
  }


  /**
   * Getter estadoAnterior
   */
  public get estadoAnterior(): Estado {
    return this._estadoAnterior;
  }

  /**
   * Setter estadoAnterior
   */
  public set estadoAnterior(value: Estado) {
    this._estadoAnterior = value;
  }

  public girarHorario() {
    switch (this.face) {
      case Face.NORTE:
        this.face = Face.NORDESTE;
        break;
      case Face.NORDESTE:
        this.face = Face.LESTE;
        break;
      case Face.LESTE:
        this.face = Face.SUDESTE;
        break;
      case Face.SUDESTE:
        this.face = Face.SUL;
        break;
      case Face.SUL:
        this.face = Face.SUDOESTE;
        break;
      case Face.SUDOESTE:
        this.face = Face.OESTE;
        break;
      case Face.OESTE:
        this.face = Face.NOROESTE;
        break;
      case Face.NOROESTE:
        this.face = Face.NORTE;
        break;
    }
  }

  public simulaGirarHorario(): Face {
    switch (this.face) {
      case Face.NORTE:
        return Face.NORDESTE;
      case Face.NORDESTE:
        return Face.LESTE;
      case Face.LESTE:
        return Face.SUDESTE;
      case Face.SUDESTE:
        return Face.SUL;
      case Face.SUL:
        return Face.SUDOESTE;
      case Face.SUDOESTE:
        return Face.OESTE;
      case Face.OESTE:
        return Face.NOROESTE;
      case Face.NOROESTE:
        return Face.NORTE;
    }
  }

  public girarAntiHorario() {
    switch (this.face) {
      case Face.NORTE:
        this.face = Face.NOROESTE;
        break;
      case Face.NORDESTE:
        this.face = Face.NORTE;
        break;
      case Face.LESTE:
        this.face = Face.NORDESTE;
        break;
      case Face.SUDESTE:
        this.face = Face.LESTE;
        break;
      case Face.SUL:
        this.face = Face.SUDESTE;
        break;
      case Face.SUDOESTE:
        this.face = Face.SUL;
        break;
      case Face.OESTE:
        this.face = Face.SUDOESTE;
        break;
      case Face.NOROESTE:
        this.face = Face.OESTE;
        break;
    }
  }

  public simulaGirarAntiHorario(): Face {
    switch (this.face) {
      case Face.NORTE:
        return Face.NOROESTE;
      case Face.NORDESTE:
        return Face.NORTE;
      case Face.LESTE:
        return Face.NORDESTE;
      case Face.SUDESTE:
        return Face.LESTE;
      case Face.SUL:
        return Face.SUDESTE;
      case Face.SUDOESTE:
        return Face.SUL;
      case Face.OESTE:
        return Face.SUDOESTE;
      case Face.NOROESTE:
        return Face.OESTE;
    }
  }

  public equals(value: Estado) {
    return (value.x === this._x
      && value.y === this._y
      && value.face === this._face);
  }
}
