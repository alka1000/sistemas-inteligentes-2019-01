export enum Face {
  NORTE,
  NORDESTE,
  LESTE,
  SUDESTE,
  SUL,
  SUDOESTE,
  OESTE,
  NOROESTE
}
