export enum Conteudo {
  VAZIO = 'Vazio',
  VISITADO = 'Visitado',
  CAMINHO_MINIMO = 'Caminho mínimo',
  OBSTACULO = 'Obstáculo',
  AGENTE = 'Agente',
  OBJETIVO = 'Objetivo'
}
