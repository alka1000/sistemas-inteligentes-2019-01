import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscaNoGridComponent } from './busca-no-grid.component';

describe('BuscaNoGridComponent', () => {
  let component: BuscaNoGridComponent;
  let fixture: ComponentFixture<BuscaNoGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscaNoGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscaNoGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
